package be.dyamand.poc.ut201blea;

/**
 * Simple model for an AnDThermometer.
 *
 * @author Christof
 */
public interface AnDThermometer {

    interface AnDListener{

        /**
         * Callback called when a new temperature value is available.
         *
         * @param newTemperatureCelcius
         */
        void onTemperatureChanged(double newTemperatureCelcius);

        /**
         * Callback called when the device radio is turned on.
         *
         * Once the radio is turned on a connection attempt will automatically be made.
         */
        void onDeviceRadioTurnedOn();

        /**
         *
         */
        void onDeviceConnected();

        void onDeviceTurnedOff();
    }

    void registerAnDListener(AnDListener listener);

    double getLastTemperature();

    boolean isConnected();


}
