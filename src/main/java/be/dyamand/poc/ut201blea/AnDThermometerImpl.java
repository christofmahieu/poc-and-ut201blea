package be.dyamand.poc.ut201blea;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.*;


import com.github.hypfvieh.DbusHelper;
import com.github.hypfvieh.bluetooth.DeviceManager;
import com.github.hypfvieh.bluetooth.wrapper.BluetoothAdapter;
import com.github.hypfvieh.bluetooth.wrapper.BluetoothDevice;
import org.bluez.Adapter1;
import org.bluez.Device1;
import org.bluez.GattCharacteristic1;
import org.bluez.exceptions.BluezFailedException;
import org.bluez.exceptions.BluezInProgressException;
import org.bluez.exceptions.BluezNotPermittedException;
import org.bluez.exceptions.BluezNotSupportedException;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.errors.UnknownObject;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.ObjectManager;
import org.freedesktop.dbus.interfaces.Properties;

/**
 * //todo document
 *
 * @author Christof
 */
public class AnDThermometerImpl implements AnDThermometer {

    private static final Logger LOGGER = Logger.getLogger(AnDThermometerImpl.class.getName());

    private static final String ADAPTER_DBUS_PATH = "/org/bluez/hci0";
    private static final String DEVICE_DBUS_PATH = "/org/bluez/hci0/dev_C8_FD_19_3A_CD_87";
    private static final String GATT_CHARACTERISTIC_DBUS_PATH = "/org/bluez/hci0/dev_C8_FD_19_3A_CD_87/service0010/char0011";

    private final AtomicBoolean connected;
    private final AtomicBoolean connecting;
    private final AtomicBoolean sending;
    private final AtomicReference<Double> temperature;
    private final Set<AnDListener> listeners;

    // DBus and BlueZ stuff
    private final DBusConnection dBusConnection;
    private final BluetoothAdapter bluetoothAdapter;
    private final Properties gattCharacteristicProperties;
    private final GattCharacteristic1 gattCharacteristic;

    public AnDThermometerImpl() throws DBusException {
        this.connected = new AtomicBoolean(false);
        this.connecting = new AtomicBoolean(false);
        this.sending = new AtomicBoolean(false);
        this.temperature = new AtomicReference<>(Double.NaN);
        this.listeners = new HashSet<>(5);

        // DBus connection
        final DeviceManager deviceManager = DeviceManager.createInstance(false);
        this.dBusConnection = deviceManager.getDbusConnection();

        // Bluetooth adapter
        final Adapter1 adapter1 =  DbusHelper.getRemoteObject(this.dBusConnection, ADAPTER_DBUS_PATH, Adapter1.class);
        this.bluetoothAdapter = new BluetoothAdapter(adapter1, ADAPTER_DBUS_PATH, this.dBusConnection);
        if(bluetoothAdapter == null){
            System.out.println("Bluetooth is turned of. Please turn it on and try again");
            System.exit(-1);
        }

        // Gatt characteristic
        //Gatt Characteristic
        this.gattCharacteristic = DbusHelper.getRemoteObject(this.dBusConnection, GATT_CHARACTERISTIC_DBUS_PATH, GattCharacteristic1.class);
        this.gattCharacteristicProperties = DbusHelper.getRemoteObject(this.dBusConnection, GATT_CHARACTERISTIC_DBUS_PATH, Properties.class);
        dBusConnection.addSigHandler(Properties.PropertiesChanged.class, this.gattCharacteristicProperties, propertiesChanged -> {
            // This callback gets called when the gatt characteristic (containing temperature information) is updated.
            LOGGER.fine("Gatt characteristic update: " + propertiesChanged);
            if(propertiesChanged.getPropertiesChanged().containsKey("Value")){
                // new temperature value
                byte[] value = (byte[])(propertiesChanged.getPropertiesChanged().get("Value").getValue());
                LOGGER.fine("New temperature data: " + Arrays.toString(value));
                this.updateTemperature(value);
            }

        });

        // Device
        final Device1 device1 = DbusHelper.getRemoteObject(this.dBusConnection, DEVICE_DBUS_PATH, Device1.class);
        final BluetoothDevice device = new BluetoothDevice(device1, this.bluetoothAdapter, DEVICE_DBUS_PATH, this.dBusConnection);
        // Note below that it is possible to add a PropertiesChanged signal listener to the device (instead of the properties
        // despite d-feet not showing an available signal to connect to. It is also necessary to connect to the device
        // directly instead of the properties.
        this.dBusConnection.addSigHandler(Properties.PropertiesChanged.class, device1, propertiesChanged -> {
            // Add a signal handler that is triggered when there is activity on the thermometer (i.e. when it
            // is done measuring and has started sending.) Activity triggers us to connect to the device
            // (connection attempts cannot be held open indefinitely, hence we wait for activity). Once
            // connected the gatt characteristic will be updated which is how we get new temperature information.
            LOGGER.fine("Device properties changed: " + propertiesChanged);
            if(!this.connecting.get() && !this.connected.get()){
                //To avoid spam we supress this message if the device is already connected or connecting
                this.listeners.forEach(AnDListener::onDeviceRadioTurnedOn);
            }
            this.connect(device);
            if(propertiesChanged.getPropertiesChanged().containsKey("Connected")){
                if( ((Boolean) propertiesChanged.getPropertiesChanged().get("Connected").getValue()) == false){
                //Device properties changed: PropertiesChanged[propertiesChanged={ ServicesResolved => [true] }, propertiesRemoved=[], interfaceName='org.bluez.Device1']
                //Device properties changed: PropertiesChanged[propertiesChanged={ ServicesResolved => [false],Connected => [false] }, propertiesRemoved=[], interfaceName='org.bluez.Device1']
                    this.setDisconnected();
                }
            }
        });
        ObjectManager objectManager = DbusHelper.getRemoteObject(this.dBusConnection, "/", ObjectManager.class);
        boolean gattCharPresent = objectManager.GetManagedObjects().keySet().stream().map(DBusPath::toString).anyMatch(p->p.equals(GATT_CHARACTERISTIC_DBUS_PATH));
        LOGGER.fine("Gatt characteristic not known yet. Waiting for it to become available.");
        if( ! gattCharPresent ){
            // The gatt characteristic isn't known yet. This happens when you reboot your laptop or turn your bluetooth
            // on or off. In this case we subscribe to the event that tells us that it is discovered.
            this.dBusConnection.addSigHandler(ObjectManager.InterfacesAdded.class, objectManager, interfaceAdded -> {
                LOGGER.fine("interface added  " + interfaceAdded);
                if(interfaceAdded.getSignalSource().toString().equals(GATT_CHARACTERISTIC_DBUS_PATH)){
                    LOGGER.fine("Gatt characteristic found. Asking it to notify");
                    try {
                        this.gattCharacteristic.StartNotify();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }else{
            this.gattCharacteristic.StartNotify();
        }

        // Start discovery --> needed to pick up device events (which triggers the connection).
        LOGGER.info("Starting discovery");
        this.bluetoothAdapter.startDiscovery();
    }

    private void connect(final BluetoothDevice device) {
        if( ! this.connecting.get() && ! this.connected.get()){
            this.connecting.set(true);
            LOGGER.fine("Connecting");
            final boolean connectionSuccess = device.connect();
            if(connectionSuccess){
                this.setConnected();
            }
            this.connecting.set(false);
            LOGGER.fine("Connection result: " + connectionSuccess);
        }
    }

    private void updateTemperature(final byte[] value) {
        byte b1 = value[0];

        // temperature unit flag
        final String temperatureUnit = ((b1 & 0x01) == 1) ? "Farenheit" : "Celcius";

        // time stamp flag
        final boolean timeStampPresent = ((b1 & 0x02)>>1 == 1);

        // temperature type flag
        final boolean temperatureTypeFlag = ((b1 & 0x04)>>2 == 1);

        final byte[] mantissaBytes = {value[1], value[2], value[3]};
        final short mantissa = ByteBuffer.wrap(mantissaBytes).order(ByteOrder.LITTLE_ENDIAN).getShort();

        double newTemperature = mantissa * Math.pow(10, value[4]);
        this.temperature.set(newTemperature);
        this.listeners.forEach(listener -> listener.onTemperatureChanged(newTemperature));
    }

    private void setConnected(){
        this.connected.set(true);
        this.listeners.forEach(AnDListener::onDeviceConnected);
    }

    private void setDisconnected(){
        this.connected.set(false);
        this.listeners.forEach(AnDListener::onDeviceTurnedOff);
    }

    @Override
    public double getLastTemperature() {
        return temperature.get();
    }

    @Override
    public void registerAnDListener(final AnDListener listener) {
        this.listeners.add(listener);
    }

    @Override public boolean isConnected() {
        return this.connected.get();
    }

    public static void main(String[] args) throws DBusException {
        System.out.println("AnD UT201BLE-A PoC application. Turn the device on and make a measurement.");
        AnDThermometer anDThermometer = new AnDThermometerImpl();
        anDThermometer.registerAnDListener(new AnDListener() {
            @Override public void onTemperatureChanged(final double newTemperatureCelcius) {
                System.out.println("New temperature value: " + newTemperatureCelcius);
            }

            @Override public void onDeviceRadioTurnedOn() {
                System.out.println("Device has turned on its radio. (auto connecting)");
            }

            @Override public void onDeviceConnected() {
                System.out.println("Device has been connected.");
            }

            @Override public void onDeviceTurnedOff() {
                System.out.println("Device has been disconnected.");
            }
        });
    }
}
